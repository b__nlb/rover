const { expect } = require('chai');
const Rover = require('../src/Rover');
const Grid = require('../src/Grid');
const moveRovers = require('../src/moveRovers');

describe('moveRovers', () => {
  it ('returns empty array if no rovers are given', () => {
    expect(moveRovers('5 5', [])).to.deep.equal([]);
  });

  it('throws an error if the top right corner is an invalid format', () => {
    expect(() => moveRovers('* 5', [])).to.throw(/invalid corner format/i);
  });

  it('throws an error if attempting to set two rovers to same location', () => {
    const rovers = [[ '1 1 N', 'LLRM' ], [ '1 1 N', 'LLR' ]];
    expect(() => moveRovers('5 5', rovers)).to.throw(/on top of an existing/i);
  });

  it('throws an error if instructions for a rover are invalid format', () => {
    const rovers = [[ '1 1 N', '~*(M' ], [ '1 1 N', 'LLR' ]];
    expect(() => moveRovers('5 5', rovers)).to.throw(/instructions are invalid/);
  });

  it('throws an error if the starting position of a rover is outside bounds', () => {
    const rovers = [[ '1 7 N', 'LRRL' ], [ '1 1 N', 'LLR' ]];
    expect(() => moveRovers('5 5', rovers)).to.throw(/outside the bounds/);
  });

  it('throws an error if attempting to move a rover into another rover', () => {
    const rovers = [[ '1 1 N', 'LRRL' ], [ '1 2 S', 'M' ]];
    expect(() => moveRovers('5 5', rovers)).to.throw(/into another rover/);
  });

  it('throws an error if attempting to move a rover off grid bounds', () => {
    const rovers = [[ '1 1 N', 'LRRL' ], [ '1 2 W', 'MMMM' ]];
    expect(() => moveRovers('5 5', rovers)).to.throw(/off a cliff/);
  });

  it('returns final positions for each rover if inputs are valid', () => {
    const rovers = [[ '1 2 N', 'LMLMLMLMM' ], [ '3 3 E', 'MMRMMRMRRM' ]];
    expect(moveRovers('5 5', rovers)).to.deep.equal([ '1 3 N', '5 1 E' ])
  });
});