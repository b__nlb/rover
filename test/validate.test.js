const { expect } = require('chai');
const { isValidPosition, areValidInstructions } = require('../src/validate');

describe('Validations', () => {
  describe('isValidPosition', () => {
    it('should return false if given too many items', () => {
      expect(isValidPosition('1 1 1 1 N E')).to.be.false;
      expect(isValidPosition('N W 4 N 9')).to.be.false;
    });

    it('should return false if x is invalid', () => {
      expect(isValidPosition('x 1 N')).to.be.false;
      expect(isValidPosition('_ 1 S')).to.be.false;
    });

    it('should return false if y is invalid', () => {
      expect(isValidPosition('1 * N')).to.be.false;
      expect(isValidPosition('9 q S')).to.be.false;
    });

    it('should return false if direction is invalid', () => {
      expect(isValidPosition('1 2 )')).to.be.false;
      expect(isValidPosition('9 1 4')).to.be.false;
    });

    it('should return true if x and y and direction are valid', () => {
      expect(isValidPosition('1 2 N')).to.be.true;
      expect(isValidPosition('1 2 S')).to.be.true;
      expect(isValidPosition('1 2 E')).to.be.true;
      expect(isValidPosition('1 2 W')).to.be.true;
    });
  });


  describe('areValidInstructions', () => {
    it('should return false if given empty string', () => {
      expect(areValidInstructions('')).to.be.false;
    });

    it('return false if it contains extra characters', () => {
      expect(areValidInstructions('LMQRLMRRRMR')).to.be.false;
      expect(areValidInstructions('X')).to.be.false;
      expect(areValidInstructions('15')).to.be.false;
      expect(areValidInstructions('_$#$@#$')).to.be.false;
    });

    it('should return false if valid characters are lowercase', () => {
      expect(areValidInstructions('lmr')).to.be.false;
      expect(areValidInstructions('lll')).to.be.false;
      expect(areValidInstructions('rrr')).to.be.false;
      expect(areValidInstructions('mmm')).to.be.false;
    });

    it('should return true if only characters are LMR', () => {
      expect(areValidInstructions('LMR')).to.be.true;
      expect(areValidInstructions('LLL')).to.be.true;
      expect(areValidInstructions('MMM')).to.be.true;
      expect(areValidInstructions('RRR')).to.be.true;
      expect(areValidInstructions('LMRLMRRRMR')).to.be.true;
      expect(areValidInstructions('L')).to.be.true;
    });
  });
});