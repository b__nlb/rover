const { expect } = require('chai');
const Grid = require('../src/Grid');
const Rover = require('../src/Rover');

describe('Grid', () => {
  let grid;

  beforeEach(() => {
    grid = new Grid({ x: 5, y: 5 });
  });

  describe('addItem', () => {
    it('should add the given item to the grid', () => {
      expect(grid.view()).to.deep.equal([]);
      grid.addItem(new Rover('1 1 N'));
      expect(grid.view()).to.deep.equal(['1 1 N']);
    });
  });

  describe('view', () => {
    it('should return an empty array if no items in grid', () => {
      expect(grid.view()).to.deep.equal([]);
    });

    it('should return an array of n items if n items in grid', () => {
      expect(grid.view().length).to.equal(0);
      grid.addItem(new Rover('1 1 N'));
      grid.addItem(new Rover('2 1 N'));
      expect(grid.view().length).to.equal(2);
    });
  });

  describe('isOccupied', () => {
    it('should return true if one of the items in the grid is at the given position', () => {
      grid.addItem(new Rover('1 1 N'));
      expect(grid.isOccupied('1 1')).to.be.true;
    });

    it('should return false if none of the items in the grid are at the given position', () => {
      grid.addItem(new Rover('1 1 N'));
      expect(grid.isOccupied('4 1')).to.be.false;
    });
  });

  describe('isWithinBounds', () => {
    it('should return true if the given position is within the grid', () => {
      expect(grid.isWithinBounds('1 1')).to.be.true;
      expect(grid.isWithinBounds('0 1')).to.be.true;
      expect(grid.isWithinBounds('1 0')).to.be.true;
      expect(grid.isWithinBounds('5 5')).to.be.true;
      expect(grid.isWithinBounds('0 0')).to.be.true;
    });

    it('should return false if the given position is outside the grid', () => {
      expect(grid.isWithinBounds('-1 1')).to.be.false;
      expect(grid.isWithinBounds('10 1')).to.be.false;
      expect(grid.isWithinBounds('100 0')).to.be.false;
    });
  });
});