const chai = require('chai');
const { expect } = chai;
const spies = require('chai-spies');
const { ACTION, DIRECTION } = require('../src/contants');
const Rover = require('../src/Rover');

// Extend chai for spies.
chai.use(spies);


describe('Rover', () => {
  describe('static methods', () => {
    describe('getNextId', () => {
      it('returns the next id', () => {
        Rover.nextId = 1;
        expect(Rover.getNextId()).to.equal(1);
        expect(Rover.getNextId()).to.equal(2);
        expect(Rover.getNextId()).to.equal(3);
        expect(Rover.getNextId()).to.equal(4);
        expect(Rover.getNextId()).to.equal(5);
      });
    });
  });


  describe('instance methods', () => {
    let rover;

    beforeEach(() => {
      const position = '1 1 N';
      const instructions = 'LRMM';
      const id = 1;
      rover = new Rover(position, instructions, id);
    });

    describe('getId', () => {
      it('returns the correct id', () => {
        expect(rover.getId()).to.equal(1);
        rover.id = 12345;
        expect(rover.getId()).to.equal(12345);
      });
    });

    describe('getPosition', () => {
      it('returns the current position of the rover', () => {
        expect(rover.getPosition()).to.deep.equal({ x: 1, y: 1 });
      });
    });

    describe('getInstructions', () => {
      it('returns the current instructions', () => {
        const expected = [ 'L', 'R', 'M', 'M' ];
        expect(rover.getInstructions()).to.deep.equal(expected);
      });
    });

    describe('getNextPosition', () => {
      it ('doesnt do anything if the current action isnt move', () => {
        const expected = rover.getPosition();
        expect(rover.getNextPosition(ACTION.ROTATE_LEFT)).to.deep.equal(expected);
        expect(rover.getNextPosition(ACTION.ROTATE_RIGHT)).to.deep.equal(expected);
      });

      describe('the action is move', () => {
        it('moves north correctly if facing north', () => {
          rover.direction = DIRECTION.N;
          const next = rover.getNextPosition(ACTION.MOVE);
          expect(next).to.deep.equal({ x: 1, y: 2 });
        });

        it('moves south correctly if facing south', () => {
          rover.direction = DIRECTION.S;
          const next = rover.getNextPosition(ACTION.MOVE);
          expect(next).to.deep.equal({ x: 1, y: 0 });
        });

        it('moves east correctly if facing east', () => {
          rover.direction = DIRECTION.E;
          const next = rover.getNextPosition(ACTION.MOVE);
          expect(next).to.deep.equal({ x: 2, y: 1 });
        });

        it('moves west correctly if facing west', () => {
          rover.direction = DIRECTION.W;
          const next = rover.getNextPosition(ACTION.MOVE);
          expect(next).to.deep.equal({ x: 0, y: 1 });
        });
      });
    });

    describe('isLocatedOn', () => {
      it('returns true if given position is where rover is located', () => {
        expect(rover.isLocatedOn({ x: 1, y: 1 })).to.be.true;
      });

      it('returns false if given position is not where rover is located', () => {
        expect(rover.isLocatedOn({ x: 100, y: -4 })).to.be.false;
      });
    });


    describe('execute', () => {
      beforeEach(() => {
        chai.spy.on(rover, 'moveForward');
        chai.spy.on(rover, 'rotateRight');
        chai.spy.on(rover, 'rotateLeft');
      });

      it('executes moveForward when action is M', () => {
        expect(rover.moveForward).to.not.have.been.called;
        rover.execute(ACTION.MOVE);
        expect(rover.moveForward).to.have.been.called;
        expect(rover.rotateRight).to.not.have.been.called;
        expect(rover.rotateLeft).to.not.have.been.called;
      });

      it('executes rotateRight when action is R', () => {
        expect(rover.rotateRight).to.not.have.been.called;
        rover.execute(ACTION.ROTATE_RIGHT);
        expect(rover.moveForward).to.not.have.been.called;
        expect(rover.rotateRight).to.have.been.called;
        expect(rover.rotateLeft).to.not.have.been.called;
      });

      it('executes rotateLeft when action is L', () => {
        expect(rover.rotateLeft).to.not.have.been.called;
        rover.execute(ACTION.ROTATE_LEFT);
        expect(rover.moveForward).to.not.have.been.called;
        expect(rover.rotateRight).to.not.have.been.called;
        expect(rover.rotateLeft).to.have.been.called;
      });

      it('executes nothing when action unknown', () => {
        rover.execute('');
        expect(rover.moveForward).to.not.have.been.called;
        expect(rover.rotateRight).to.not.have.been.called;
        expect(rover.rotateLeft).to.not.have.been.called;
      });
    });


    describe('executeAll', () => {
      beforeEach(() => {
        chai.spy.on(rover, 'execute');
      });

      it('calls execute on each item in instructions', () => {
        const expected = rover.getInstructions().length;
        expect(rover.execute).to.not.have.been.called;
        rover.executeAll();
        expect(rover.execute).to.have.been.called.exactly(expected);
      });

      it('calls nothing if no instructions', () => {
        rover.instructions = [];
        rover.executeAll();
        expect(rover.execute).to.not.have.been.called;
      });
    });

    describe('moveForward', () => {
      it('sets the rover\'s position to the next forward position', () => {
        expect(rover.getPosition()).to.deep.equal({ x: 1, y: 1 });
        rover.moveForward();
        expect(rover.getPosition()).to.deep.equal({ x: 1, y: 2 });
      });
    });

    describe('rotateLeft', () => {
      it('rotates the rover left', () => {
        expect(rover.direction).to.equal(DIRECTION.N);
        rover.rotateLeft();
        expect(rover.direction).to.equal(DIRECTION.W);
      });
    });

    describe('rotateRight', () => {
      it('rotates the rover right', () => {
        expect(rover.direction).to.equal(DIRECTION.N);
        rover.rotateRight();
        expect(rover.direction).to.equal(DIRECTION.E);
      });
    });

    describe('view', () => {
      it('returns a string representation of the rover state', () => {
        expect(rover.view()).to.equal('1 1 N');
      });
    });
  });
});