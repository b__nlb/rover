
const DIRECTION = {
  N: 'N',
  S: 'S',
  E: 'E',
  W: 'W',
};

const ROTATION = {
  LEFT: 'Left',
  RIGHT: 'Right',
};

const ACTION = {
  MOVE: 'M',
  ROTATE_LEFT: 'L',
  ROTATE_RIGHT: 'R',
};

// Allows us to change what delimiters are used for inputs.
const DELIMITER = {
  INSTRUCTION: '',
  POSITION: ' ',
};


module.exports = {
  DIRECTION,
  ROTATION,
  ACTION,
  DELIMITER,
};
