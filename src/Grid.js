/* eslint-disable no-param-reassign */
const { DELIMITER } = require('./contants');


/** Class representing the plateau. */
class Grid {
  // '1 2' -> { x: 1, y: 2 }
  static convertToObject(position) {
    const [x, y] = position.split(DELIMITER.POSITION).map(n => +n);
    return { x, y };
  }


  // Allow string or object to be entered.
  // e.g. '1 2' or { x: 1, y: 2 }
  static getFinalPosition(position) {
    return typeof position === 'string'
      ? Grid.convertToObject(position)
      : position;
  }


  constructor(topRight = { x: 1, y: 1 }, bottomLeft = { x: 0, y: 0 }) {
    this.bottomLeft = bottomLeft;
    this.topRight = Grid.getFinalPosition(topRight);
    this.items = [];
  }


  addItem(item) {
    this.items.push(item);
  }


  getItems() {
    return this.items;
  }


  // Return an array of item positions.
  view() {
    return this.items.map(item => item.view());
  }


  // True if the grid has an item at the given position in place already.
  isOccupied(position) {
    position = Grid.getFinalPosition(position);
    const { x, y } = position;

    return this.items.reduce((isOccupied, item) => {
      const itemPosition = item.getPosition();
      if (itemPosition.x === x && itemPosition.y === y) {
        isOccupied = true;
      }
      return isOccupied;
    }, false);
  }


  // True if the given position is within the bound of the board.
  isWithinBounds(position) {
    position = Grid.getFinalPosition(position);
    const { x, y } = position;

    return (
      (x >= this.bottomLeft.x && x <= this.topRight.x) &&
      (y >= this.bottomLeft.y && y <= this.topRight.y)
    );
  }
}


module.exports = Grid;
