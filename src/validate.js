const { DELIMITER } = require('./contants');
const Grid = require('./Grid');
const Rover = require('./Rover');

/**
 * Validates all starting positions and instructions.
 *
 * @param {string} topRightCorner The top right position of the grid.
 * @param {Array.<string[]>} rovers Starting positions and instructions for each rover.
 * @throws Will throw an error if any inputs will damage a rover.
 * @returns {undefined} Returns without throwing error if inputs are valid.
 */
function validateInstructions(topRightCorner, rovers) {
  if (!isValidCorner(topRightCorner)) {
    throw new Error(`
      Invalid corner format. Please use format 'x y'
      You entered: ${topRightCorner}
    `);
  }

  // Initialize a grid with the size of our board.
  // It keeps a list of rovers that exist on the board and allows us
  // to know if a rover is about to crash into another one, or if a rover
  // is about to fall off a cliff. A rover doesn't know about any other
  // rovers.
  const grid = new Grid(topRightCorner);

  // Attempt to add each rover to our grid. If any of the inputs are invalid
  // or off the grid, we throw an error instead of executing any actions on
  // the rovers.
  rovers.forEach(([startingPosition, instructions], index) => {
    // Allow lower case instructions.
    // eslint-disable-next-line no-param-reassign
    instructions = instructions.toUpperCase();

    // Used to make error messages more helpful so that the person can see
    // which rover had invalid inputs.
    const roverId = index + 1;

    // Give the user some helpful data so they don't have to come back here
    // and start logging/debugging in the case of an error.
    const specifics = `
      You entered: ${startingPosition}
      for rover #${roverId}
    `;

    if (!isValidPosition(startingPosition)) {
      throw new Error(`
        Invalid starting position for rover. Please use the format: 'x y'
        ${specifics}
      `);
    }

    if (!areValidInstructions(instructions)) {
      throw new Error(`
        Given instructions are invalid. Please enter only combinations of:
          'L', 'M', or 'R'
          e.g. 'LMLMLMLMRRM'
        ${specifics}
      `);
    }

    if (!grid.isWithinBounds(startingPosition)) {
      throw new Error(`
        The starting position for a rover is outside the bounds of the plateau.
        ${specifics}
      `);
    }

    if (grid.isOccupied(startingPosition)) {
      throw new Error(`
        You attempted to set a rover on top of an existing rover.
        ${specifics}
      `);
    }

    grid.addItem(new Rover(startingPosition, instructions, roverId));
  });

  // All of our rovers have been added to the grid at this point without issue.
  // Now we need to check the instructions for each of them to ensure we don't
  // have instructions that run them off cliffs or into each other.
  grid.getItems().forEach((rover) => {
    rover.getInstructions()
      .forEach((action) => {
        const nextPosition = rover.getNextPosition(action);
        const isNewPosition = !rover.isLocatedOn(nextPosition);

        const state = `
          Grid state: ${grid.view()},
          Rover number: ${rover.getId()}
          Rover state: ${rover.view()}
        `;

        if (grid.isOccupied(nextPosition) && isNewPosition) {
          throw new Error(`
            You're attempting to steer a rover into another rover!
            ${state}
          `);
        }

        if (!grid.isWithinBounds(nextPosition)) {
          throw new Error(`
            You're attempting to steer a rover off a cliff!
            ${state}
          `);
        }

        // Don't update the rover until we know we're not going to damage it.
        rover.execute(action);
      });
  });

  // Not returning anything, since we're synchronously running through each
  // instruction for each rover and throwing an error if we have an issue.
  // The calling code knows the instructions lead to a solution if this function
  // returns without throwing an error.
}


// Helpers


const getInputs = inputs => inputs.split(DELIMITER.POSITION);
const isDigit = char => /^\d+$/.test(char);
const isDirection = char => /^[NSEW]$/.test(char);


/**
 * Validates that a position is in the format 'x y d' where x & y are integers
 * and d is a valid NSEW direction.
 *
 * @param {string} position The starting position of a rover.
 * @returns {boolean} True if inputs are valid.
 */
function isValidPosition(position) {
  const inputs = getInputs(position);
  const [x, y, direction] = inputs;

  return inputs.length === 3
    && isDigit(x)
    && isDigit(y)
    && isDirection(direction);
}


/**
 * Validates that a corner is in the format 'x y' where x & y are integers;
 *
 * @param {string} corner The top right corner of the grid.
 * @returns {boolean} True if inputs are valid.
 */
function isValidCorner(corner) {
  const inputs = getInputs(corner);
  const [x, y] = inputs;
  return inputs.length === 2 && isDigit(x) && isDigit(y);
}

/**
 * Validates that instructions are valid.
 *
 * @param {string} instructions The instructions to be sent to a rover.
 * @returns {boolean} True if inputs are valid.
 */
function areValidInstructions(instructions) {
  return /^[MLR]+$/.test(instructions);
}


module.exports = {
  validateInstructions,
  isValidPosition,
  areValidInstructions,
};
