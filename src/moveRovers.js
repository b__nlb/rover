const Grid = require('./Grid');
const Rover = require('./Rover');
const { validateInstructions } = require('./validate');

// This solution makes a couple of assumptions about how the rovers are
// deployed and how they should behave:
//
//   1) All rovers are placed on the plateau before they start moving.
//      i.e. If two rovers are placed on the plateau, they'll both exist on
//      the plateau at the same time, before any instructions are parsed.
//      We're not behaving as if the first one lands on an empty plateau,
//      moves around, and then the second one lands, and so on.
//
//   2) Each call to this function is treated as a single transaction.
//      Either the inputs are valid, and all of the rovers follow
//      the given instructions, or none of the rovers are moved.
//      Space is a big place and latency would be a big issue.
//      This prevents a scientist from receiving an error after half the
//      rovers have moved. Better to know up front that the inputs are
//      invalid or would damage a rover before wasting resources. This is
//      less efficient than handling errors as we go, but much safer.
//
//   3) Any instructions that would result in the destruction of a rover
//      (falling off the plateau, running into an existing rover, etc)
//      will result in the transaction failing and an error message being
//      sent to the scientist.

/**
 * Get the final position of the rovers.
 *
 * @param {string} topRightCorner The top right position of the grid.
 * @param {Array.<string[]>} rovers Starting positions and instructions for each rover.
 * @throws Will throw an error if any inputs will damage a rover.
 * @returns {Array.<string>} The position of each rover after it's been moved.
 */
function moveRovers(topRightCorner, rovers) {
  // Ensure that the given inputs can result in a solution.
  // This will throw an error if any of the inputs are invalid or
  // would damage a rover.
  validateInstructions(topRightCorner, rovers);

  // Each rover only knows about itself, so the grid keeps track
  // of each rover that exists on the plateau.
  const grid = new Grid(topRightCorner);

  // Add each rover to the grid before we begin moving them.
  rovers.forEach(([startingPosition, instructions]) => {
    grid.addItem(new Rover(startingPosition, instructions));
  });

  // Since we know the rover's instructions won't result in a collision
  // or any damage, execute all actions to move each rover to its final
  // location.
  grid.getItems().forEach(rover => rover.executeAll());

  return grid.view();
}


// Play around with inputs here.

const topRight = '50 50';

const rovers = [
  ['1 2 N', 'LMLMLMLMM'],
  ['3 3 E', 'MMRMMRMRRM'],
];

const answer = moveRovers(topRight, rovers);

// eslint-disable-next-line no-console
console.log(answer);


module.exports = moveRovers;
