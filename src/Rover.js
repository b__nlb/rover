const {
  DIRECTION, ROTATION, ACTION, DELIMITER,
} = require('./contants');

/** Class representing a rover. */
class Rover {
  constructor(initialPosition, instructions = '', id = Rover.getNextId()) {
    const { position, direction } = Rover.getFromString(initialPosition);
    this.position = position;
    this.direction = direction;
    this.instructions = instructions.toUpperCase().split(DELIMITER.INSTRUCTION);
    this.id = id;
  }


  // Each time a rover is created, give it a new id.
  // This is helpful for error messages.
  static getNextId() {
    return Rover.nextId++;
  }


  // '1 1 N' -> { position: { x: 1, y: 1 }, direction: 'N' }
  static getFromString(initial) {
    const [x, y, direction] = initial.split(DELIMITER.POSITION);

    return {
      position: { x: +x, y: +y },
      direction,
    };
  }


  getId() {
    return this.id;
  }


  getPosition() {
    return { x: this.position.x, y: this.position.y };
  }


  getInstructions() {
    return this.instructions;
  }


  getNextPosition(action) {
    if (action !== ACTION.MOVE) return this.position;

    /* eslint-disable no-param-reassign */
    const getNext = {
      [DIRECTION.N]: ({ x, y }) => ({ x, y: ++y }),
      [DIRECTION.S]: ({ x, y }) => ({ x, y: --y }),
      [DIRECTION.E]: ({ x, y }) => ({ x: ++x, y }),
      [DIRECTION.W]: ({ x, y }) => ({ x: --x, y }),
    }[this.direction];
    /* eslint-enable no-param-reassign */

    return getNext(this.position);
  }


  isLocatedOn({ x, y }) {
    return x === this.position.x && y === this.position.y;
  }


  // eslint-disable-next-line consistent-return
  execute(action) {
    // eslint-disable-next-line default-case
    switch (action) {
      case ACTION.MOVE:
        return this.moveForward();
      case ACTION.ROTATE_LEFT:
        return this.rotateLeft();
      case ACTION.ROTATE_RIGHT:
        return this.rotateRight();
    }
  }


  executeAll() {
    this.instructions.forEach(action => this.execute(action));
  }


  moveForward() {
    this.position = this.getNextPosition(ACTION.MOVE);
  }


  rotateLeft() {
    return this._rotate(ROTATION.LEFT);
  }


  rotateRight() {
    return this._rotate(ROTATION.RIGHT);
  }


  view() {
    return [
      this.position.x,
      this.position.y,
      this.direction,
    ].join(DELIMITER.POSITION);
  }


  _rotate(to) {
    const directions = [DIRECTION.N, DIRECTION.E, DIRECTION.S, DIRECTION.W];
    const currentIndex = directions.indexOf(this.direction);

    if (to === ROTATION.LEFT) {
      this.direction = currentIndex === 0
        ? directions[directions.length - 1]
        : directions[currentIndex - 1];
    }

    if (to === ROTATION.RIGHT) {
      this.direction = currentIndex === directions.length - 1
        ? directions[0]
        : directions[currentIndex + 1];
    }
  }
}


// Give the first rover to be created an id of one.
Rover.nextId = 1;


module.exports = Rover;
