
# Setup:

  1. [Install node.js](https://nodejs.org/en/)
  4. Install project dependencies. `npm install`

# To run tests or lint:

  1. Run `npm run test` to run all unit tests.
  2. Run `npm run lint` to run eslint with Airbnb config.

# To play around:

  1. Open up './src/moveRovers.js'.
  2. The function is invoked at the bottom and logs out the result.
  3. Update inputs as desired and save.
  3. Run `npm run go` to run the program.
